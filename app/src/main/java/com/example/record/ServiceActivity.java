/*
package com.example.record;


import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.record.base.BaseFragmentActivity;
import com.example.record.fragment.FirstFragment;
import com.jpeng.jptabbar.JPTabBar;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;


import com.qmuiteam.qmui.arch.QMUISwipeBackActivityManager;
import com.qmuiteam.qmui.arch.annotation.DefaultFirstFragment;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.xuexiang.xui.XUI;

import java.util.HashMap;
import java.util.Map;


@DefaultFirstFragment(FirstFragment.class)
public class ServiceActivity extends BaseFragmentActivity {


    private int[] mTitles = {R.string.tab1, R.string.tab2, R.string.tab3, R.string.tab4};
    private int[] mSelectIcons = {R.drawable.nav_01_pre, R.drawable.nav_02_pre, R.drawable.nav_04_pre, R.drawable.nav_05_pre};
    private int[] mNormalIcons = {R.drawable.nav_01_nor, R.drawable.nav_02_nor, R.drawable.nav_04_nor, R.drawable.nav_05_nor};

    private Map<String, View> mPageMap = new HashMap<>();
    private PagerAdapter mPagerAdapter = new PagerAdapter() {
        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @Override
        public int getCount() {
            return mTitles.length;
        }

        @Override
        public Object instantiateItem(final ViewGroup container, int position) {
            View view = getPageView(getString(mTitles[position]));
            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            container.addView(view, params);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }
    };


    public int getContextViewId() {
        XUI.init(this.getApplication());
        QMUIStatusBarHelper.translucent(this);
        QMUIStatusBarHelper.setStatusBarDarkMode(this);
        QMUISwipeBackActivityManager.init(this.getApplication());

        return R.layout.activity_service;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_service);
        JPTabBar viewById = findViewById(R.id.btabbar);
        ViewPager viewPager = findViewById(R.id.view_pager);


        initViews(viewById,viewPager);

        super.onCreate(savedInstanceState);
    }
    */
/**
     * 初始化控件
     *//*


    protected void initViews(JPTabBar mTabbar,ViewPager mViewPager) {
        mTabbar.setTitles(mTitles);
        mTabbar.setNormalIcons(mNormalIcons);
        mTabbar.setSelectedIcons(mSelectIcons);
        mTabbar.generate();
        //页面可以滑动
        mTabbar.setGradientEnable(true);
        mTabbar.setPageAnimateEnable(true);

        mViewPager.setAdapter(mPagerAdapter);
        mTabbar.setContainer(mViewPager);

        if (mTabbar.getMiddleView() != null) {
            mTabbar.getMiddleView().setOnClickListener(v -> System.out.println("中间点击"));
        }

        mTabbar.showBadge(2, "", true);
    }
    private View getPageView(String pageName) {
        View view = mPageMap.get(pageName);
        if (view == null) {
            TextView textView = new TextView(this);
            textView.setGravity(Gravity.CENTER);
            textView.setText(String.format("这个是%s页面的内容", pageName));
            view = textView;
            mPageMap.put(pageName, view);
        }
        return view;
    }
}*/
