package com.example.record.login;

import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import com.example.record.R;
import com.example.record.base.BaseFragment;
import com.example.record.fragment.ServiceActivity;
import com.qmuiteam.qmui.widget.QMUITopBarLayout;
import com.qmuiteam.qmui.widget.QMUIWindowInsetLayout;
import com.qmuiteam.qmui.widget.roundwidget.QMUIRoundButton;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class LoginFragment extends BaseFragment {

    @BindView(R.id.loginSignIn)
    QMUIRoundButton signIn;
    @BindView(R.id.loginRegister)
    QMUIRoundButton register;
    @BindView(R.id.topbar)
    protected QMUITopBarLayout topbar;
    @BindView(R.id.userName)
    EditText userName;
    @BindView(R.id.password)
    EditText password;

    @Override
    protected View onCreateView() {
        QMUIWindowInsetLayout layout = (QMUIWindowInsetLayout) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_login, null);
        unbinder = ButterKnife.bind(this, layout);
        initTopBar(topbar, "登  录");
        return layout;
    }

    @OnClick({R.id.loginRegister})
    public void registerClick() {
        RegisterFragment registerFragment = new RegisterFragment();
        startFragment(registerFragment);
    }

    @OnClick({R.id.loginSignIn})
    public void loginClick() {


        try {

            startFragment(new ServiceActivity());
            onDestroy();
            /*Intent in = new Intent();
            in.setClass(this.getContext(), ServiceActivity.class);
            startActivity(in);*/


           /* List<User> query = DBUtils.getDao(User.class)
                    .queryBuilder()
                    .where()
                    .eq("user_name", userName.getText().toString())
                    .and()
                    .eq("password", password.getText().toString())
                    .query();
            if (query.size() > 0) {
                Intent intent = new Intent();
                intent.setClass(this.getContext(), ServiceActivity.class);
                startActivity(intent);
            } else {
                success("用户名或密码错误");
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}