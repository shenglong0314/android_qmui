package com.example.record;



import com.example.record.base.BaseFragmentActivity;
import com.example.record.login.LoginFragment;
import com.example.record.utils.DBUtils;
import com.qmuiteam.qmui.arch.QMUISwipeBackActivityManager;
import com.qmuiteam.qmui.arch.annotation.DefaultFirstFragment;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;


@DefaultFirstFragment(LoginFragment.class)
public class MainActivity extends BaseFragmentActivity {
    @Override
    public int getContextViewId() {
        QMUIStatusBarHelper.translucent(this);
        QMUIStatusBarHelper.setStatusBarDarkMode(this);
        QMUISwipeBackActivityManager.init(this.getApplication());
        DBUtils.init(this);
        return R.layout.activity_main;
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}