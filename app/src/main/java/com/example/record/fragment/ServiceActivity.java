package com.example.record.fragment;



import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.record.R;
import com.example.record.base.BaseFragment;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.util.QMUIResHelper;
import com.qmuiteam.qmui.widget.QMUITopBarLayout;
import com.qmuiteam.qmui.widget.QMUIWindowInsetLayout;
import com.qmuiteam.qmui.widget.tab.QMUITab;
import com.qmuiteam.qmui.widget.tab.QMUITabBuilder;
import com.qmuiteam.qmui.widget.tab.QMUITabSegment;


import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ServiceActivity extends BaseFragment {


    private int[] mTitles = {R.string.tab1, R.string.tab2, R.string.tab3, R.string.tab4};
    private int[] mSelectIcons = {R.drawable.nav_01_pre, R.drawable.nav_02_pre, R.drawable.nav_04_pre, R.drawable.nav_05_pre};
    private int[] mNormalIcons = {R.drawable.nav_01_nor, R.drawable.nav_02_nor, R.drawable.nav_04_nor, R.drawable.nav_05_nor};

    //private Map<ContentPage, View> mPageMap = new HashMap<>();

    private PagerAdapter mPagerAdapter = new PagerAdapter() {
        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public int getCount() {
            return Pager.getcount();
        }

        @Override
        public Object instantiateItem(final ViewGroup container, int position) {
            Pager page = Pager.getPagerFromPosition(position);
            View view = getPageView(page);
            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            container.addView(view, params);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

    };
   // private HashMap<Pager, HomeController> mPages;
   private Map<Pager, View> mPageMap = new HashMap<>();
    @BindView(R.id.topbar)
    protected QMUITopBarLayout topbar;


    @BindView(R.id.tabSegment)
    QMUITabSegment mTabSegment;
    @BindView(R.id.contentViewPager)
    ViewPager mContentViewPager;


    @Override
    protected View onCreateView() {
        QMUIWindowInsetLayout layout = (QMUIWindowInsetLayout) LayoutInflater.from(getActivity()).inflate(R.layout.activity_service, null);


        unbinder = ButterKnife.bind(this, layout);
        initTopBar(topbar,"service");


        initTabs();
        initPagers();
        return layout;
    }
    private void initTabs() {

        QMUITabBuilder builder = mTabSegment.tabBuilder();
        //builder.setTypeface(null, Typeface.DEFAULT_BOLD);
        builder.setSelectedIconScale(1.2f)
                .setTextSize(QMUIDisplayHelper.sp2px(getContext(), 13), QMUIDisplayHelper.sp2px(getContext(), 15))
                .setDynamicChangeIconColor(false)
                 .setNormalIconSizeInfo(44,44);
        builder.skinChangeWithTintColor(false);


        Drawable n1 = ContextCompat.getDrawable(getContext(), R.drawable.nav_01_nor);
        Drawable p1 = ContextCompat.getDrawable(getContext(), R.drawable.nav_01_pre);
        QMUITab component = builder
                .setGravity(Gravity.CENTER)
                .setNormalDrawable(n1)
                .setSelectedDrawable(p1)
                .setText("Components")
                .build(getContext());
        QMUITab util = builder
                .setNormalDrawable(ContextCompat.getDrawable(getContext(),R.drawable.nav_02_nor))
                .setSelectedDrawable(ContextCompat.getDrawable(getContext(), R.drawable.nav_02_pre))
                .setText("Helper")
                .build(getContext());
        QMUITab lab = builder
                .setNormalDrawable(ContextCompat.getDrawable(getContext(), R.drawable.nav_04_nor))
                .setSelectedDrawable(ContextCompat.getDrawable(getContext(), R.drawable.nav_04_pre))
                .setText("Lab")
                .build(getContext());
        QMUITab a = builder
                .setNormalDrawable(ContextCompat.getDrawable(getContext(), R.drawable.nav_05_nor))
                .setSelectedDrawable(ContextCompat.getDrawable(getContext(), R.drawable.nav_05_pre))
                .setText("aa")
                .build(getContext());

        mTabSegment.addTab(component)
                .addTab(util)
                .addTab(lab)
                .addTab(a);
        mTabSegment.notifyDataChanged();
    }
    private void initPagers() {

        mContentViewPager.setAdapter(mPagerAdapter);
        mTabSegment.setMode(QMUITabSegment.MODE_FIXED);
        mTabSegment.setupWithViewPager(mContentViewPager, false);
    }


    enum Pager {
        COMPONENT, UTIL, LAB,AA;

        public static Pager getPagerFromPosition(int position) {
            switch (position) {
                case 0:
                    return COMPONENT;
                case 1:
                    return UTIL;
                case 2:
                    return LAB;
                case 3:
                    return AA;
                default:
                    return COMPONENT;
            }
        }
        public static int getcount(){

            return Pager.values().length;
        }
    }

    private View getPageView(Pager page) {
        View view = mPageMap.get(page);
        if (view == null) {
            TextView textView = new TextView(getContext());
            textView.setGravity(Gravity.CENTER);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            textView.setTextColor(ContextCompat.getColor(getContext(), R.color.app_color_description));

            if (page == Pager.COMPONENT) {
                textView.setText( "COMPONENT");
            } else if (page == Pager.UTIL) {
                textView.setText("util");
            } else if(page == Pager.LAB){
                textView.setText("LAB");
            }else {
                textView.setText("aa");
            }

            view = textView;
            mPageMap.put(page, view);
        }
        return view;
    }
}