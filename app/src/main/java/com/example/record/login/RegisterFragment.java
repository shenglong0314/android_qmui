package com.example.record.login;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.record.R;
import com.example.record.base.BaseFragment;
import com.example.record.empty.User;
import com.example.record.utils.DBUtils;
import com.qmuiteam.qmui.widget.QMUITopBarLayout;
import com.qmuiteam.qmui.widget.QMUIWindowInsetLayout;
import com.qmuiteam.qmui.widget.roundwidget.QMUIRoundButton;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterFragment extends BaseFragment {


    @BindView(R.id.register)
    QMUIRoundButton register;
    @BindView(R.id.topbar)
    protected QMUITopBarLayout topbar;
    @BindView(R.id.r_userName)
    EditText userName;
    @BindView(R.id.r_password)
    EditText password;
    @Override
    protected View onCreateView() {
        QMUIWindowInsetLayout layout = (QMUIWindowInsetLayout) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_register, null);
        unbinder = ButterKnife.bind(this, layout);
        initTopBar();

        return layout;
    }
    protected void initTopBar() {
        topbar.addLeftBackImageButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popBackStack();
            }
        });
        super.initTopBar(topbar,"注 册");
    }

    @OnClick({R.id.register})
    public void registerClick(){

        try {
            List<User> query = DBUtils.getDao(User.class)
                    .queryBuilder()
                    .where()
                    .eq("user_name", userName.getText().toString())
                    .and()
                    .eq("password", password.getText().toString())
                    .query();
            if(query.size()>0){
                success("用户已被注册，请重新注册");
                return;
            }

            User user = new User();
            user.setUserName(userName.getText().toString());
            user.setPassword(password.getText().toString());
            int i = DBUtils.getDao(User.class).create(user);
            if(i>0){
                success("注册成功");
            }else {
                success("注册失败");
            }



        }catch (Exception e){e.printStackTrace();}



    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}