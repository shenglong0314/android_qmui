package com.example.record.fragment;

import android.view.LayoutInflater;
import android.view.View;
import com.example.record.R;
import com.example.record.base.BaseFragment;
import com.qmuiteam.qmui.widget.QMUITopBarLayout;
import com.qmuiteam.qmui.widget.QMUIWindowInsetLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SecondFragment extends BaseFragment {

    @BindView(R.id.topbar)
    protected QMUITopBarLayout topbar;
    @Override
    protected View onCreateView() {
        QMUIWindowInsetLayout layout = (QMUIWindowInsetLayout) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_second, null);
        unbinder = ButterKnife.bind(this, layout);
        initTopBar(topbar,"记  录");
        return layout;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}