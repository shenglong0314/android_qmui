package com.example.record.utils;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.record.empty.User;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DBUtils {
    private static Db db;

    public static void  init(Context context){
         db = new Db(context);
    }
    public static <T> Dao<T,Integer> getDao(Class<T> clazz){

        try {
            Dao<T,Integer> dao = db.getDao(clazz);
            return dao;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }


    static class Db extends OrmLiteSqliteOpenHelper {
        public Db(Context context){
            super(context,"record",null,1);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
            try {
                TableUtils.clearTable(connectionSource, User.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i1) {
            try {
                TableUtils.dropTable(connectionSource,User.class,true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
